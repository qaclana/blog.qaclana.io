---
layout: post
title: Website is live!
description: The first version of our website is now live
---

We have just finished filling up the blank spaces on the website template, with
basic information about our project and published it live on
[qaclana.io](http://www.qaclana.io ). The next steps are, in no particular order:

* Get a minimally decent logo, visual identity
* Publish our first version, along with instructions on how to use it
* Refactor the website

If you want to help, be it on the website, be it on coding, be it on producing
a logo/visual identity, [contact us](https://chat.qaclana.io/) !
