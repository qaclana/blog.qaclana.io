---
layout: post
title: Getting started
description: How to get started with Qaclana
tags: tutorial howto getting-started tips
---

At this stage, Qaclana is still a prototype and not ready for high concurrent,
production environments. It doesn't even do much, but as the distribution
channel is now built, these instructions shouldn't change much. For those brave
enough who want to take a look at what this is all about, here is a (hopefully)
comprehensive set of instructions on how to get started with Qaclana.

# Overview

Qaclana is split into several modules, each with its own capabilities. Those
are then assembled into higher level modules, such as `backend`, `server` or
`proxy`. Another key component is the `filter`, which is embedded into the
`proxy`.

# Running all services

Qaclana is not distributed as a single package, but as several smaller units.
As such, the easiest way to get it all running is to use Docker:
```
docker run -d --name=qaclana-proxy -p 8080:8080 qaclana/qaclana-proxy
docker run -d --name=qaclana-server -p 8081:8081 qaclana/qaclana-server
docker run -d --name=qaclana-backend -p 8082:8082 qaclana/qaclana-backend
```

You can check that all services are up by checking the logs:
```
docker logs qaclana-proxy
docker logs qaclana-server
docker logs qaclana-backend
```

# Configuring the proxy

Qaclana comes with some sensible defaults for most options, but it cannot
know in advance what is the target application for the proxy. As such, the
`qaclana-proxy` service has to be started with either a specific Configuration
file, or with a specific "entry" command. Similarly, the other services can
also be changed by changing the entry command, overriding the path to the
configuration file, or mounting the whole `conf` directory.

## Changing the entry command

The easiest is to specify the `-t` parameter, like this:
```
docker run \
  -d \
  -p 8080:8080 \
  -v /path/on/host/for/conf:/conf \
  --name qaclana-proxy \
  qaclana/qaclana-proxy \
  /app/bin/qaclana-proxy -t http://target.example.com
```

## Overriding the path to the configuration file

If more configuration options have to be changed, it might be easier to mount
the configuration directory somewhere, and override configuration file path
with the `-c` option:
```
docker run \
  -d \
  -p 8080:8080 \
  -v /path/on/host/for/conf:/conf \
  --name qaclana-proxy \
  qaclana/qaclana-proxy \
  /app/bin/qaclana-proxy -c /conf/configuration.yml
```

## Mounting the whole `conf` directory

Or even, it's possible to mount the whole `/conf` over the one from the image:
```
docker run \
  -d \
  -p 8080:8080 \
  -v /path/on/host/for/conf:/app/conf \
  --name qaclana-proxy \
  qaclana/qaclana-proxy
```
